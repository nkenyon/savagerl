﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelData {
    public TileData tileData;
    public Vector3 spawnPoint;

    public LevelData(TileType[][] tiles, Vector3 spawnPoint)
    {
        tileData.tiles = tiles;
        this.spawnPoint = spawnPoint;
    }
}
