﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class IntRange {

    public int m_Min;
    public int m_Max;

    public IntRange(int min, int max)
    {
        this.m_Min = min;
        this.m_Max = max;
    }

    public int Random
    {
        get { return UnityEngine.Random.Range(m_Min, m_Max); }
    }
	
}
