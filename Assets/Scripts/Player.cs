﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



public class Player : MonoBehaviour {
    public Camera cam;
    public Vector3 cameraOffset;
    public float moveSpeed;
    public PlayerData playerData;

    // Use this for initialization
    void Start() {
        cam = FindObjectOfType<Camera>();
        LoadData();
    }

    // Update is called once per frame
    void Update() {
        
    }

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        Vector3 velocity = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0 );
        this.transform.position += velocity * Time.deltaTime * moveSpeed;
        cam.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z) + cameraOffset;
    }

    void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + "playerSaveFile.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            playerData = (PlayerData)bf.Deserialize(file);
            file.Close();
        }
    }

    void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "playerSaveFile.gd");
        bf.Serialize(file, playerData);
        file.Close();
    }

    void DeleteData()
    {

    }

    void OnCollisionEnter(Collision col)
    {
        try
        {
            Stairs stair = col.gameObject.GetComponent<Stairs>();
            if (stair.goesDown)
            {
                SaveData();
                StartNextLevelLoad();
            }
        }
        catch(System.NullReferenceException e)
        {
            Debug.LogWarning(e.Message);
        }
    }

    void StartNextLevelLoad()
    {
        UIController uic = FindObjectOfType<UIController>();
        uic.inLevelTransition = true;
    }
   

}
