﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum ViewState
{
    FadeToBlack, FadeFromBlack, Waiting, Showing
}

public class UIController : MonoBehaviour {
    public bool inLevelTransition;
    public GameObject statsBlock;
    public GameObject blackoutScreen;
    public float blackoutOpacity = 1.0f;
    public float fadeAmount;

    ViewState viewState = ViewState.FadeFromBlack;

	// Use this for initialization
	void Start () {
        StartFade(ViewState.FadeFromBlack);
	}
	
	// Update is called once per frame
	void Update () {
        if(viewState == ViewState.FadeFromBlack)
        {
            FadeFromBlack();
        }
        if(viewState == ViewState.FadeToBlack)
        {
            FadeToBlack();
        }
        if(viewState == ViewState.Waiting)
        {
            inLevelTransition = true;
        }

        if (viewState == ViewState.Showing)
        {
            if (Input.GetKeyUp(KeyCode.K))
            {
                statsBlock.SetActive(!statsBlock.activeSelf);
            }
        }
	}

    void FadeFromBlack()
    {
        if (blackoutOpacity <= 0.0f)
        {
            viewState = ViewState.Showing;
            blackoutScreen.SetActive(false);
        }

        else
        {
            Color preColor = blackoutScreen.GetComponent<Image>().color;
            Color newColor = new Color(preColor.r, preColor.g, preColor.b, blackoutOpacity);
            blackoutScreen.GetComponent<Image>().color = newColor;
            blackoutOpacity -= fadeAmount * Time.deltaTime;
        }
    }

    void FadeToBlack()
    {

        if (blackoutOpacity >= 1.0f)
        {
            viewState = ViewState.Waiting;
        }

        else
        {
            inLevelTransition = true;
            Color preColor = blackoutScreen.GetComponent<Image>().color;
            Color newColor = new Color(preColor.r, preColor.g, preColor.b, blackoutOpacity);
            blackoutScreen.GetComponent<Image>().color = newColor;
            blackoutOpacity += fadeAmount * Time.deltaTime;
        }
    }

    void StartFade(ViewState newState)
    {
        viewState = newState;
        blackoutScreen.SetActive(true);
    }

    
}
