﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerData {
    public string name;

    public int savageScore;
    public int level;
    public int experience;
    public int upgradePoints;
    public int gold;
    public int food;
    public int herbs;

    //primary stats
    public int strength;
    public int agility;
    public int vitality;
    public int attunement;
    public int genius;
    public int ego;

    //secondary stats
    public int health;

    public int getStatValue(Stat stat)
    {
        int result = -1;
        switch (stat)
        {
            case Stat.Strength:
                result = strength;
                break;

            case Stat.Agility:
                result = agility;
                break;

            case Stat.Vitality:
                result = vitality;
                break;

            case Stat.Attunement:
                result = attunement;
                break;

            case Stat.Genius:
                result = genius;
                break;

            case Stat.Ego:
                result = ego;
                break;

            default:
                result = -1;
                break;
        }
        return result;
    }

    public void ModifyStat(Stat stat, int amount)
    {
        if (getStatValue(stat) + amount > 0)
        {
            switch (stat)
            {
                case Stat.Strength:
                    strength += amount;
                    break;

                case Stat.Agility:
                    agility += amount;
                    break;

                case Stat.Vitality:
                    vitality += amount;
                    break;

                case Stat.Attunement:
                    attunement += amount;
                    break;

                case Stat.Genius:
                    genius += amount;
                    break;

                case Stat.Ego:
                    ego += amount;
                    break;

                default:
                    break;
            }
        }
    }

    public void PromoteStat(Stat stat)
    {
        if (upgradePoints - 1 >= 0)
        {
            ModifyStat(stat, 1);
            upgradePoints--;
        }
    }

    public void DemoteStat(Stat stat)
    {
        ModifyStat(stat, -1);
        upgradePoints++;
    }
}

public enum Stat
{
    Strength, Agility, Vitality, Attunement, Genius, Ego
}

public class StatNames
{

    public static string getStatName(Stat stat)
    {
        string result = "STAT TRANSLATION ERROR";
        switch (stat)
        {
            case Stat.Strength:
                result = "Strength";
                break;

            case Stat.Agility:
                result = "Agility";
                break;

            case Stat.Vitality:
                result = "Vitality";
                break;

            case Stat.Attunement:
                result = "Attunement";
                break;

            case Stat.Genius:
                result = "Genius";
                break;

            case Stat.Ego:
                result = "Ego";
                break;

            default:
                result = "STAT TRANSLATION ERROR";
                break;
        }
        return result;
    }


}
