﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TileData {
    public TileType[][] tiles;
}
