﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatButton : MonoBehaviour {
    public Stat stat;
    public Player player;
    public Text statName;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
        statName = this.transform.GetChild(0).GetComponent<Text>();
        SetNameText();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void IncreaseStat()
    {
        player.playerData.PromoteStat(stat);
        SetNameText();
    }

    public void DecreaseStat()
    {
        player.playerData.DemoteStat(stat);
        SetNameText();
    }

    public void SetNameText()
    {
        statName.text = StatNames.getStatName(stat) + ": " + player.playerData.getStatValue(stat);
    }
}
