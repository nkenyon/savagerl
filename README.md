# README #

This is the readme for savageRL. I'm new to writing readme pages so please bear with me as I try to get better at this.

# What is this repository for? #

* This is for hosting the code for my rogue-like, savageRL
* Currently on version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You will need [Unity](https://store.unity.com/download?ref=personal)
* For right now, just open the project in Unity and then use the engine's "run" button to play the scenes. I haven't stitched them together yet, which is also why there isn't a build you can just download and play

### Contribution guidelines ###

* This game is being made by myself (Nicklas Kenyon) and Jacob Nesbitt.

### Who do I talk to? ###

* If you have problems, hit up the [issue tracker](https://bitbucket.org/nkenyon/savagerl/issues) and I'll try to deal with things as quickly as a full college plate will allow.